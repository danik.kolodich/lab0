﻿using System;

namespace task2 {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Enter positive num : ");
            uint num = uint.Parse(Console.ReadLine());
            string res = string.Empty;
            for (; num != 0;) {
                res += num % 2 == 1 ? "1" : "0";
                num /= 2;
            }

            char[] tmp = res.ToCharArray();
            Array.Reverse(tmp);
            res = new string(tmp);
            Console.WriteLine(res);
        }
    }
}
