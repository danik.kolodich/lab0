﻿using System;

namespace task1 {
    class Program {
        static void Main(string[] args) {
            double epsilon, num, x0, x1;
            int root;
            Console.WriteLine("Enter num : ");
            num = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("\nEnter epsilon (#,###): ");
            epsilon = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("\nEnter root : ");
            root = Convert.ToInt32(Console.ReadLine());
            x1 = num / 2;
            int it = 0;
            double res = Math.Pow(num, 1d / root);
            do {
                it++;
                x0 = x1;
                x1 = (1d / root) * ((root - 1d) * x0 + num / Math.Pow(x0, root - 1));
            }
            while (Math.Abs(x0 - x1) > epsilon);

            Console.WriteLine($"\nRes {x1} & correct value {res} & it {it}");
        }
    }
}
